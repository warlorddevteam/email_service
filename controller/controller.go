package controller

import (
	"gopkg.in/gomail.v2"
)

//Sending user short pass to email
func sendShortPassToEmail(c Conf, to string, code string) {
	g := gomail.NewDialer(c.EmailHost, c.EmailPort, c.EmailLogin, c.EmailPassword)
	g.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	m := gomail.NewMessage()
	m.SetHeader("From", c.EmailLogin)
	m.SetHeader("To", to)
	m.SetHeader("Subject", "Security")
	m.SetHeader("Subject", fmt.Sprintf("Hello! its %v", c.AppName))
	m.SetBody("text/html", fmt.Sprintf("Hello your pass is <b>%v</b>!", code))
	err := g.DialAndSend(m)
	if err != nil {
		panic(err)
	}
	fmt.Println("ok sended")
}